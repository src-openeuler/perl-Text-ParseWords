%define mod_name Text-ParseWords

Name:		perl-%{mod_name}
Version:	3.31
Release:	2
Summary:	Parse text into an array of tokens or array of arrays
License:	GPL-1.0-or-later OR Artistic-1.0-Perl
URL:		https://metacpan.org/release/%{mod_name}
Source0:	https://cpan.metacpan.org/authors/id/N/NE/NEILB/%{mod_name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:	perl-interpreter perl-generators
BuildRequires:	perl(ExtUtils::MakeMaker) perl(Test::More)
Requires:	perl(Carp)

%description
The &nested_quotewords() and &quotewords() functions accept a delimiter (which
can be a regular expression) and a list of lines and then breaks those lines up
into a list of words ignoring delimiters that appear inside quotes. &quotewords()
returns all of the tokens in a single long list, while &nested_quotewords()
returns a list of token lists corresponding to the elements of @lines.
&parse_line() does tokenizing on a single string. The &*quotewords() functions
simply call &parse_line(), so if you're only splitting one line you can call
&parse_line() directly and save a function call.

The $keep argument is a boolean flag. If true, then the tokens are split on the
specified delimiter, but all other characters (including quotes and backslashes)
are kept in the tokens. If $keep is false then the &*quotewords() functions
remove all quotes and backslashes that are not themselves backslash-escaped or
inside of single quotes (i.e., &quotewords() tries to interpret these characters
just like the Bourne shell). NB: these semantics are significantly different
from the original version of this module shipped with Perl 5.000 through 5.004.
As an additional feature, $keep may be the keyword "delimiters" which causes the
functions to preserve the delimiters in each string as tokens in the token lists,
in addition to preserving quote and backslash characters.

%package_help

%prep
%setup -q -n %{mod_name}-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=%{buildroot} NO_PACKLIST=1
%{_fixperms} %{buildroot}

%check
make test

%files
%{perl_vendorlib}/*

%files help
%doc CHANGES README
%{_mandir}/man3/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 3.31-2
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Jan 14 2025 sqfu <dev01203@linx-info.com> - 3.31-1
- update version to 3.31
- Skip taint tests if running under Perl that doesn't support taint
- Doc: restructured section describing the $keep parameter
- Doc: dropped the & in front of function names, as it felt old-school
- Doc: updated the AUTHORS section

* Mon Oct 24 2022 hongjinghao <hongjinghao@huawei.com> - 3.30-420
- add mod_name macro

* Tue Sep 17 2019 luhuaxin <luhuaxin@huawei.com> - 3.30-419
- Fix bug: Add buildrequire perl-generators

* Wed Sep 11 2019 luhuaxin <luhuaxin@huawei.com> - 3.30-418
- Package init
